/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

//import KeywordDrivenTestFramework.Testing.PageObjects.Air_Quality_Monitoring.*;
//import KeywordDrivenTestFramework.Testing.PageObjects.Audit_PageObjects.*;
import KeywordDrivenTestFramework.Testing.PageObjects.ResponsibilityMatrix_PageObjects.*;
import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author
 */
public class ResponsibilityMatrix_PageObjects extends BaseClass
{
    public static String addButtonXpath()
    {
        return "//div[@id='btnActAddNew']";
    }
     
    public static String processFlow()
    {
        return "//div[@id='btnProcessFlow_form_A7EEB62D-CF26-42FB-8480-B6982189833A']";
    }

    public static String processFlowStatus(String phase)
    {
    return "(//div[text()='"+phase+"'])[2]/parent::div";
    }
    
    public static String processFlowStatusChild(String phase)
    {
    return "(//div[text()='"+phase+"'])[3]/parent::div";
    }
    
    public static String responsibleDepartmentDropdown()
    {
        return "//div[@id='control_86D9ACCA-4140-40B6-A30B-B0C64A7128AD']//li";
    }  
       
    public static String singleSelect(String value)
    {
        return "//div[contains(@class, 'transition visible')]//a[text()='"+value+"']";
    } 
    
    public static String activityDropdown()
    {
        return "//div[@id='control_E929DB1C-FE45-4BAA-A402-D6D999B21B61']//li";
    }
    
    public static String frequencyCheckbox()
    {
       return "//div[@id='control_8571EE0B-E976-4E57-A4C2-7A30ABC53D79']//div[@class='c-chk']";
    }
    
    public static String frequencyDropdown()
    {
        return "//div[@id='control_5566AEFA-A6A4-4634-B986-490914F70DF1']//li";
    }
    
    public static String onWhichWeekDayLabel()
    {
        return "//div[@id='control_1F96D16C-9552-4B95-9171-80B0CB659AF4']";
    }
    
    public static String onWhichWeekDayDropdown()
    {
        return "//div[@id='control_6D8D1DBD-8873-4130-81C7-523F26A98F8E']//li";
    }
    
    public static String dutyTextbox()
    {
       return "(//div[@id='control_BFFC2D99-68F0-4EDA-A956-1FDAB64FE4D0']//input)[1]";
    }
   
    public static String responsibleCompanyDropdown()
    {
        return "//div[@id='control_BCFD6240-05A3-4113-9169-E0765DB1B84C']//li";
    }
    
    public static String natureOfDutyDropdown()
    {
        return "//div[@id='control_D674C012-FF7F-4853-ABA5-7309AAB95A5B']//li";
    }
    
    public static String responsiblePersonDropdown()
    {
        return "//div[@id='control_983A79CE-1B34-43F3-AE10-D895108E240A']//li";
    }
    
    public static String rotationOrLeavePersonDropdown()
    {
        return "//div[@id='control_46AE60DE-1DBA-4213-8C2A-2AC1F22C7756']//li";
    }
        
    public static String dutySignoffPanel()
    {
        return "//span[text()='Duty Sign - Off']";
    }
    
    public static String dutySignedOffDropdown()
    {
        return "//div[@id='control_266934E2-CEF5-4A6D-A293-C338B3693BF3']//li";
    }
    
    public static String commentsLabel()
    {
        return "//div[@id='control_4A404EF0-061D-4DB8-845E-D6D774AC2F5C']";
    }
    
    public static String commentsTextbox()
    {
        return "//div[@id='control_A43828D5-2C44-4D2E-A264-38FE866EFADD']//textarea";
    }
        
    public static String saveButton()
    {
        return " //div[@id='btnSave_form_A7EEB62D-CF26-42FB-8480-B6982189833A']";
    } 
   
    public static String validateSave() {
        return "//div[@class='ui floating icon message transition visible']";
    }

    public static String saveWait(){
        return ".//div[text()='Saving...']";
    }

    public static String taskCompletionLogGrid()
    {
        return " //div[@id='control_80352015-224B-4D8C-BCB7-417C1EB5C591']";
    }
    
    public static String respMatrixRecordNumber_xpath() {
        return "(//div[@id='form_A7EEB62D-CF26-42FB-8480-B6982189833A']//div[contains(text(),'- Record #')])[1]";
    }
    
    //FR1 OS
    public static String uploadDocument()
    {
         return "//div[@id='control_15E6020B-C125-4912-AC93-AC9002D9437A']//b[@original-title='Upload a document']";
    }

    public static String linkToADocument()
    {
          return "//div[@id='control_15E6020B-C125-4912-AC93-AC9002D9437A']//b[@original-title='Link to a document']";
    }

    public static String LinkURL()
    {
         return "//div[@class='confirm-popup popup']//input[@id='urlValue']";
    }

    public static String urlTitle()
    {
         return "//div[@class='popup-container']//input[@id='urlTitle']";
    }

    public static String urlAddButton()
    {
           return "//div[@class='popup-container']//div[contains(text(),'Add')]";
    }

    public static String iframeName()
    {
          return "ifrMain";
    }
     
    //FR1 AS2
    public static String weekOfTheMonthLabel()
    {
        return "//div[@id='control_F4CEB016-B13A-47EF-8FD7-4C3FC4F84A0E']";
    }
    
    public static String monthlyFromLabel()
    {
        return "//div[@id='control_1E789879-D5EB-4897-AAC3-B0D731006E8A']";
    }
    
    public static String weekOfTheMonthDropdown()
    {
        return "//div[@id='control_75368206-BE2D-49FD-B2E6-3AE6807B4293']//li";
    }
    
    public static String monthlyFromDropdown()
    {
        return "//div[@id='control_230D1133-F505-409C-A6DD-0BF672BBE5A0']//li";
    }
    
    public static String reasonLabel()
    {
        return "//div[@id='control_85CB6331-57D2-46A3-8AE0-1F46DB48ADB5']";
    }

    public static String reasonTextbox()
    {
        return "//div[@id='control_A43828D5-2C44-4D2E-A264-38FE866EFADD']//textarea";
    }

    
    //FR3 MS
    public static String taskComplnLogAddButtonXpath() {
        return "//div[@id='control_80352015-224B-4D8C-BCB7-417C1EB5C591']//div[@id='btnAddNew']";
    }
    
    public static String taskCompletedDropdown()
    {
        return "//div[@id='control_F185FCFE-7663-4BF5-8827-32016E7C55DA']//li";
    }
    
    public static String signedOffBy()
    {
        return "//div[@id='control_09D6DABE-CEF4-4220-BEFD-D762AA219B58']//li";
    }
    
    
            
}
