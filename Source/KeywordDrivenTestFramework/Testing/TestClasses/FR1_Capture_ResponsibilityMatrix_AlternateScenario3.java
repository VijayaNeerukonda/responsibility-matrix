/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.ResponsibilityMatrix_PageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "FR1_Capture_ResponsibilityMatrix_AlternateScenario3",
    createNewBrowserInstance = false
)

public class FR1_Capture_ResponsibilityMatrix_AlternateScenario3 extends BaseClass
{

    String error = "";
    private String textbox;
   

  public FR1_Capture_ResponsibilityMatrix_AlternateScenario3()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!addResponsibilityMatrixRecord())
        {
            return narrator.testFailed("Failed to add Responsibility Matrix record: " + error);
        }
        return narrator.finalizeTest("Successfully added Responsibility Matrix record");
    }

   
    public boolean addResponsibilityMatrixRecord() throws InterruptedException
    {    
        //Add button
         pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.addButtonXpath()))
        {
            error = "Failed to locate Responsibility Matrix add button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.addButtonXpath()))
        {
            error = "Failed to click on Responsibility Matrix add button";
            return false;
        }
        
        //processflow
        pause(5000);
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.processFlow()))
        {
            error = "Failed to locate processflow button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.processFlow()))
        {
            error = "Failed to click on processflow";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Processflow in Add phase");
        String processStatusAdd = SeleniumDriverInstance.retrieveAttributeByXpath(ResponsibilityMatrix_PageObjects.processFlowStatus("Add phase"),"class");
        if (!processStatusAdd.equalsIgnoreCase("step active")) {
        error = "Failed to be in Add phase";
        return false;
        }
        
        //Responsible department Dropdown
         if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.responsibleDepartmentDropdown()))
        {
            error = "Failed to locate Responsible department Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.responsibleDepartmentDropdown()))
        {
            error = "Failed to click on Responsible department Dropdown";
            return false;
        }
    
         
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Responsible department"))))
        {
            error = "Failed to locate Responsible department Dropdown value";
            return false;
        }   
      
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Responsible department"))))
        {
            error = "Failed to click Responsible department Dropdown value";
            return false;
        }
        
        //Activity Dropdown
         if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.activityDropdown()))
        {
            error = "Failed to locate Activity Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.activityDropdown()))
        {
            error = "Failed to click on Activity Dropdown";
            return false;
        }
    
         
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Activity"))))
        {
            error = "Failed to locate Activity Dropdown value";
            return false;
        }   
      
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Activity"))))
        {
            error = "Failed to click Activity Dropdown value";
            return false;
        }
       
        
        //Frequency Checkbox
        if (getData("Frequency Checkbox").equalsIgnoreCase("TRUE")) {
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.frequencyCheckbox()))
        {
            error = "Failed to locate Frequency Checkbox";
            return false;
        }   
      
         if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.frequencyCheckbox()))
        {
            error = "Failed to click Frequency Checkbox";
            return false;
        }
        }else {
            narrator.stepPassedWithScreenShot("Sucessfully didnot select Frequency checkbox");
        }
        
        //Frequency Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.frequencyDropdown()))
        {
            error = "Failed to locate Frequency Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.frequencyDropdown()))
        {
            error = "Failed to click on Frequency Dropdown";
            return false;
        }
    
       
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Frequency"))))
        {
            error = "Failed to locate Frequency Dropdown value";
            return false;
        }   
      
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Frequency"))))
        {
            error = "Failed to click Frequency Dropdown value";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("No additional fields are trigerred");
              
        
        //Duty textbox
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.dutyTextbox()))
        {
            error = "Failed to locate Duty field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ResponsibilityMatrix_PageObjects.dutyTextbox(),testData.getData("Duty")))
        {
            error = "Failed to enter text in Duty field";
            return false;
        }
        
        //Responsible Company
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.responsibleCompanyDropdown()))
        {
            error = "Failed to locate Responsible Company Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.responsibleCompanyDropdown()))
        {
            error = "Failed to click on Responsible Company Dropdown";
            return false;
        }
    
        
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Responsible Company"))))
        {
            error = "Failed to locate Responsible Company Dropdown value";
            return false;
        }   
      
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Responsible Company"))))
        {
            error = "Failed to click Responsible Company Dropdown value";
            return false;
        }
        
        //Nature of Duty dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.natureOfDutyDropdown()))
        {
            error = "Failed to locate Nature of Duty Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.natureOfDutyDropdown()))
        {
            error = "Failed to click on Nature of Duty Dropdown";
            return false;
        }
    
       
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Nature of Duty"))))
        {
            error = "Failed to locate Nature of Duty Dropdown value";
            return false;
        }   
      
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Nature of Duty"))))
        {
            error = "Failed to click Nature of Duty Dropdown value";
            return false;
        }
        
        //Responsible Person
       if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.responsiblePersonDropdown()))
        {
            error = "Failed to locate Responsible Person Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.responsiblePersonDropdown()))
        {
            error = "Failed to click on Responsible Person Dropdown";
            return false;
        }
    
        
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Responsible Person"))))
        {
            error = "Failed to locate Responsible Person Dropdown value";
            return false;
        }   
      
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Responsible Person"))))
        {
            error = "Failed to click Responsible Person Dropdown value";
            return false;
        }
        
        //Rotation/Leave Person
       if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.rotationOrLeavePersonDropdown()))
        {
            error = "Failed to locate Rotation/Leave Person Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.rotationOrLeavePersonDropdown()))
        {
            error = "Failed to click on Rotation/Leave Person Dropdown";
            return false;
        }
    
         
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Rotation/Leave Person"))))
        {
            error = "Failed to locate Rotation/Leave Person Dropdown value";
            return false;
        }   
      
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Rotation/Leave Person"))))
        {
            error = "Failed to click Rotation/Leave Person Dropdown value";
            return false;
        }
        
        //Duty Signoff Panel
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.dutySignoffPanel()))
        {
            error = "Failed to locate Duty Signoff Panel";
            return false;
        }   
      
         if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.dutySignoffPanel()))
        {
            error = "Failed to click Duty Signoff Panel";
            return false;
        } 
        
        //Duty Signed Off Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.dutySignedOffDropdown()))
        {
            error = "Failed to locate Duty Signed Off Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.dutySignedOffDropdown()))
        {
            error = "Failed to click on Duty Signed Off Dropdown";
            return false;
        }
    
        
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Duty Signed Off"))))
        {
            error = "Failed to locate Duty Signed Off Dropdown value";
            return false;
        }   
      
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Duty Signed Off"))))
        {
            error = "Failed to click Duty Signed Off Dropdown value";
            return false;
        }
        
        //Reason textbox
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.reasonTextbox()))
        {
            error = "Failed to locate Reason field";
            return false;
        }

        if (!SeleniumDriverInstance.enterTextByXpath(ResponsibilityMatrix_PageObjects.commentsTextbox(),testData.getData("Reason")))
        {
            error = "Failed to enter text in Reason field";
            return false;
        }
        
//        String retrieveTextField4 = SeleniumDriverInstance.retrieveTextByXpath(ResponsibilityMatrix_PageObjects.reasonLabel()); 
//        if (!retrieveTextField4.equalsIgnoreCase("Reason")) {
//                error = "Failed to retrieve Reason label text ";
//                return false;
//            }
        
        //Save button
         if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }   
      
         if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        } 
        
         if (!SeleniumDriverInstance.WaitForLoaderToDisappear(ResponsibilityMatrix_PageObjects.saveWait())) {
            error = "Failed to wait for Saving…";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(ResponsibilityMatrix_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(ResponsibilityMatrix_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        narrator.stepPassedWithScreenShot("Successfully saved the record and processflow moves to Edit phase");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(ResponsibilityMatrix_PageObjects.respMatrixRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());   
        
        String processStatusEdit = SeleniumDriverInstance.retrieveAttributeByXpath(ResponsibilityMatrix_PageObjects.processFlowStatus("Edit phase"),"class");
        if (!processStatusEdit.equalsIgnoreCase("step active")) {
        error = "Failed to move to Edit phase";
        return false;
        }
        
        narrator.stepPassedWithScreenShot("Task Completion Log non editable grid is displayed");    
//        String retrieveGridText = SeleniumDriverInstance.retrieveTextByXpath(ResponsibilityMatrix_PageObjects.taskCompletionLogGrid()); 
//        if (!retrieveGridText.equalsIgnoreCase("Task Completion Log")) {
//                error = "Failed to retrieve Task Completion Log grid text ";
//                return false;
//            }
        
        return true;
        
        

        
    }

}
