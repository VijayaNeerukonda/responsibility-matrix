/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.ResponsibilityMatrix_PageObjects;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;
import java.io.File;
import org.openqa.selenium.JavascriptExecutor;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "FR1_Capture_ResponsibilityMatrix_OptionalScenario",
    createNewBrowserInstance = false
)

public class FR1_Capture_ResponsibilityMatrix_OptionalScenario extends BaseClass
{

    String error = "";
    private String textbox;
   
  SikuliDriverUtility sikuliDriverUtility;
    String pathofImages, path;
    
  public FR1_Capture_ResponsibilityMatrix_OptionalScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!uploadSupportingDocuments())
        {
            return narrator.testFailed("Failed to upload Supporting Documents " + error);
        }
        return narrator.finalizeTest("Successfully uploaded Supporting Documents");
    }

   
    public boolean uploadSupportingDocuments() throws InterruptedException
    {    
        //Scroll down
        JavascriptExecutor js = (JavascriptExecutor)SeleniumDriverInstance.Driver;
        js.executeScript("window.scrollBy(0,250)");
        
        //Click upload document
//        if(!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.uploadDocument())){
//            error = "Failed to wait for Supporting documents 'Upload document' link.";
//            return false;
//        }
//        
//        if(!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.uploadDocument())){
//            error = "Failed to click on Supporting documents 'Upload document' link.";
//            return false;
//        }
//
//        if(!SeleniumDriverInstance.uploadAFile("C:\\Users\\Vijaya\\Documents\\Vijaya Docs\\sample document.xlsx")){
//        error = "Failed to upload Supporting documents";
//        return false;
//        }

        //Upload hyperlink to Supporting documents
        //click link
        if(!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.linkToADocument())){
            error = "Failed to wait for Supporting documents 'Link box' link.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.linkToADocument())){
            error = "Failed to click on Supporting documents 'Link box' link.";
            return false;
        }
        pause(1000);
        narrator.stepPassedWithScreenShot("Successfully clicked Supporting documents 'Upload Hyperlink box'.");

        //switch to new window
        if(!SeleniumDriverInstance.switchToTabOrWindow()){
            error = "Failed to switch to new window or tab.";
            return false;
        }

        //URL https
        if(!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.LinkURL())){
            error = "Failed to wait for 'URL value' field.";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(ResponsibilityMatrix_PageObjects.LinkURL(), getData("Document Link") )){
            error = "Failed to enter '" + getData("Document Link") + "' into 'URL value' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Document link : '" + getData("Document Link") + "'.");

        //Title
        if(!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.urlTitle())){
            error = "Failed to wait for 'Url Title' field.";
            return false;
        }
        
        if(!SeleniumDriverInstance.enterTextByXpath(ResponsibilityMatrix_PageObjects.urlTitle(), getData("URL Title"))){
            error = "Failed to enter '" + getData("URL Title") + "' into 'Url Title' field.";
            return false;
        }
        narrator.stepPassedWithScreenShot("URL Title : '" + getData("URL Title") + "'.");

        //Add button
        if(!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.urlAddButton())){
            error = "Failed to wait for 'Add' button.";
            return false;
        }
        
        if(!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.urlAddButton())){
            error = "Failed to click on 'Add' button.";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Successfully clicked the 'Add' button.");
        narrator.stepPassed("Successfully uploaded '" + getData("Title") + "' document using '" + getData("Document Link") + "' Link.");

        //Switch to iframe
        if (!SeleniumDriverInstance.swithToFrameByName(ResponsibilityMatrix_PageObjects.iframeName()))
        {
            error = "Failed to switch to frame ";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully switched the iframe.");


        //Save button
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.saveButton())) {
            error = "Failed to locate Save button";
            return false;
        }

        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        }

         if (!SeleniumDriverInstance.WaitForLoaderToDisappear(ResponsibilityMatrix_PageObjects.saveWait())) {
            error = "Failed to wait for Saving…";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(ResponsibilityMatrix_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(ResponsibilityMatrix_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");


        narrator.stepPassedWithScreenShot("Successfully saved supporting documents");

        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(ResponsibilityMatrix_PageObjects.respMatrixRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
        return true;
        
        

        
    }

}
