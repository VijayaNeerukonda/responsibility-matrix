/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.ResponsibilityMatrix_PageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "FR3_ViewandAdd_TaskCompletionLog_MainScenario",
    createNewBrowserInstance = false
)

public class FR3_ViewandAdd_TaskCompletionLog_MainScenario extends BaseClass
{

    String error = "";
    private String textbox;
   

  public FR3_ViewandAdd_TaskCompletionLog_MainScenario()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!updateTaskCompletionLog())
        {
            return narrator.testFailed("Failed to update Task Completion Log: " + error);
        }
        return narrator.finalizeTest("Successfully updated Task Completion Log");
    }

   
    public boolean updateTaskCompletionLog() throws InterruptedException
    {    
        //Add button
         pause(3000);
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.taskComplnLogAddButtonXpath()))
        {
            error = "Failed to locate Task Completion Log add button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.taskComplnLogAddButtonXpath()))
        {
            error = "Failed to click on Task Completion Log add button";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Task Completed defaulted to No");
//        String retrieveTextField1 = SeleniumDriverInstance.retrieveTextByXpath(ResponsibilityMatrix_PageObjects.taskCompletedDropdown()); 
//        if (!retrieveTextField1.equalsIgnoreCase("No")) {
//                error = "Failed to retrieve Task Completed dropdown value ";
//                return false;
//            }
         
        //Task Completed Dropdown
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.taskCompletedDropdown()))
        {
            error = "Failed to locate Task Completed Dropdown";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.taskCompletedDropdown()))
        {
            error = "Failed to click on Task Completed Dropdown";
            return false;
        }
    
         
        if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Task Completed"))))
        {
            error = "Failed to locate Task Completed Dropdown value";
            return false;
        }   
      
        if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.singleSelect(testData.getData("Task Completed"))))
        {
            error = "Failed to click Task Completed Dropdown value";
            return false;
        }
        
        narrator.stepPassedWithScreenShot("Signed off by field is updated by Logged in User, Date Completed updated to current date");      
//        String retrieveTextField2 = SeleniumDriverInstance.retrieveTextByXpath(ResponsibilityMatrix_PageObjects.signedOffBy()); 
//        if (!retrieveTextField2.equalsIgnoreCase("2 Manager")) {
//                error = "Failed to retrieve Signed off by dropdown value";
//                return false;
//            }
        
       
        //Save button
         if (!SeleniumDriverInstance.waitForElementByXpath(ResponsibilityMatrix_PageObjects.saveButton()))
        {
            error = "Failed to locate Save button";
            return false;
        }   
      
         if (!SeleniumDriverInstance.clickElementbyXpath(ResponsibilityMatrix_PageObjects.saveButton()))
        {
            error = "Failed to click Save button";
            return false;
        } 
        
         if (!SeleniumDriverInstance.WaitForLoaderToDisappear(ResponsibilityMatrix_PageObjects.saveWait())) {
            error = "Failed to wait for Saving…";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(ResponsibilityMatrix_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(ResponsibilityMatrix_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + " : successfully.");

        narrator.stepPassedWithScreenShot("Successfully saved the record and processflow remains in Edit phase");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(ResponsibilityMatrix_PageObjects.respMatrixRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());   
      
        return true;
        
        

        
    }

}
