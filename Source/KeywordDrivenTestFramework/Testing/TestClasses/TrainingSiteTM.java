/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.TrainingSiteTMPageObjects;

/**
 *
 * @author vijaya
 */

@KeywordAnnotation
(
    Keyword = "Click on Tailings Management",
    createNewBrowserInstance = false
)
public class TrainingSiteTM extends BaseClass
{

    String error = "";

    public TrainingSiteTM()
    {

    }

    public TestResult executeTest() throws InterruptedException
    {
        if (!navigateToAPageFromTailingsManagementPage())
        {
            return narrator.testFailed("Failed to navigate to a module from Tailings Management page" + error);
        }
        return narrator.finalizeTest("Successfully Navigated to module from Tailings Management page");
    }

   
    public boolean navigateToAPageFromTailingsManagementPage() throws InterruptedException
    {
       
        if (!SeleniumDriverInstance.waitForElementByXpath(TrainingSiteTMPageObjects.linkForAPageInHomePageXpath(testData.getData("Tailings ManagementPageName")))) {
            error = "Failed to locate the module: "+testData.getData("Tailings ManagementPageName");
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(TrainingSiteTMPageObjects.linkForAPageInHomePageXpath(testData.getData("Tailings ManagementPageName"))))
        {
            error = "Failed to navigate to the module: "+testData.getData("Tailings ManagementPageName");
            return false;
        }

        
        return true;

    }

}
