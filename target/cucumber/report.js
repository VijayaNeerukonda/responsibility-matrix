$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Sample/SampleFeature.feature");
formatter.feature({
  "line": 1,
  "name": "Sample Feature",
  "description": "",
  "id": "sample-feature",
  "keyword": "Feature"
});
formatter.scenario({
  "line": 4,
  "name": "1 - This is an example of a scenario",
  "description": "",
  "id": "sample-feature;1---this-is-an-example-of-a-scenario",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@SmokeTest"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I am a Jira user",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "I run my automated tests",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "QTest Manager should show the results",
  "keyword": "Then "
});
formatter.match({
  "location": "SampleStepDefs.i_am_a_Jira_user()"
});
formatter.result({
  "duration": 64408733,
  "status": "passed"
});
formatter.match({
  "location": "SampleStepDefs.i_run_my_automated_tests()"
});
formatter.result({
  "duration": 16318,
  "status": "passed"
});
formatter.match({
  "location": "SampleStepDefs.qtest_Manager_should_show_the_results()"
});
formatter.result({
  "duration": 1153638,
  "error_message": "java.lang.AssertionError\r\n\tat org.junit.Assert.fail(Assert.java:86)\r\n\tat org.junit.Assert.fail(Assert.java:95)\r\n\tat BDD.step_definitions.SampleStepDefs.qtest_Manager_should_show_the_results(SampleStepDefs.java:37)\r\n\tat ✽.Then QTest Manager should show the results(Sample/SampleFeature.feature:7)\r\n",
  "status": "failed"
});
});